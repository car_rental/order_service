package service

import (
	"context"
	"fmt"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/xuri/excelize/v2"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/car_rental/order_service/config"
	"gitlab.com/car_rental/order_service/genproto/car_service"
	"gitlab.com/car_rental/order_service/genproto/order_service"
	"gitlab.com/car_rental/order_service/genproto/user_service"
	"gitlab.com/car_rental/order_service/grpc/client"
	"gitlab.com/car_rental/order_service/pkg/logger"
	"gitlab.com/car_rental/order_service/storage"
)

type ExcelFields struct {
	ClientName    string
	PhoneNumber   string
	OrderUniqueId string
	BranchName    string
	BrandName     string
	Modelname     string
	StateNumber   string
	InvestorName  string
	Duration      int64
	TarifName     string
	Dercription   string
	TarifPrice    int64
	InsuranceSum  int64
	DiscountSum   int64
	InvestorSum   int64
	TotalSum      int64
	CompanySum    int64
	Cash          int64
	Uzcard        int64
	Humo          int64
	Payme         int64
	Click         int64
	Status        string
}

type OrderService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedOrderServiceServer
}

func NewOrderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *OrderService {
	return &OrderService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *OrderService) Create(ctx context.Context, req *order_service.OrderCreate) (*order_service.Order, error) {
	u.log.Info("====== Order Create ======", logger.Any("req", req))

	resp, err := u.strg.Order().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Order: u.strg.Order().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderService) GetById(ctx context.Context, req *order_service.OrderPrimaryKey) (*order_service.Order, error) {
	u.log.Info("====== Order Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Order().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Order Get By ID: u.strg.Order().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderService) GetList(ctx context.Context, req *order_service.OrderGetListRequest) (*order_service.OrderGetListResponse, error) {
	u.log.Info("====== Order Get List ======", logger.Any("req", req))

	resp, err := u.strg.Order().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Order Get List: u.strg.Order().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderService) Update(ctx context.Context, req *order_service.OrderUpdate) (*order_service.Order, error) {
	u.log.Info("====== Order Update ======", logger.Any("req", req))

	resp, err := u.strg.Order().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Order Update: u.strg.Order().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// Get OrderTarif Info

	order_tarif_info, err := u.strg.OrderTarif().GetByID(ctx, &order_service.OrderTarifPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Error While Update OrderPayment: u.strg.OrderTarif().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	if req.Status == "canceled" {
		// Here we Update Order Status
		_, err = u.strg.OrderTarif().Update(ctx, &order_service.OrderTarifUpdate{
			Id:            order_tarif_info.Id,
			OrderId:       order_tarif_info.OrderId,
			TariffPayment: order_tarif_info.TariffPayment,
			Investor:      order_tarif_info.Investor,
			Insurance:     order_tarif_info.Insurance,
			Company:       order_tarif_info.Company,
			Day:           order_tarif_info.Day,
			Status:        "canceled",
		})
		if err != nil {
			u.log.Error("Error While Update OrderPayment: u.strg.OrderTarif().Update", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		// Here we Update Order Status
		_, err = u.strg.Order().Update(ctx, &order_service.OrderUpdate{
			Id:             req.Id,
			TarifId:        req.TarifId,
			ClientId:       req.ClientId,
			BranchId:       req.BranchId,
			CarId:          req.CarId,
			Date:           req.Date,
			Duration:       req.Duration,
			ExtendDuration: req.ExtendDuration,
			Discount:       req.Discount,
			Insurance:      req.Insurance,
			Status:         req.Status,
			OrderUniqueId:  req.OrderUniqueId,
			Paid:           "returned To Client",
		})
		if err != nil {
			u.log.Error("Error While Create OrderPayment: u.strg.Order().Update", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
	}

	return resp, nil
}

func (u *OrderService) Delete(ctx context.Context, req *order_service.OrderPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Order Delete ======", logger.Any("req", req))

	err := u.strg.Order().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Order Delete: u.strg.Order().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}

func (u *OrderService) Download(ctx context.Context, req *order_service.ExcelRequest) (*order_service.ExcelResponse, error) {
	u.log.Info("====== Excel Download ======")
	var Excel = []ExcelFields{}
	var infos = ExcelFields{}

	orders, err := u.strg.Order().GetList(ctx, &order_service.OrderGetListRequest{})
	if err != nil {
		u.log.Error("Error While Download: u.strg.Order().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	for _, order := range orders.Orders {
		u.log.Info("We In For Loop")
		// Get Client Name and Phone Number
		client, err := u.services.ClientService().GetById(ctx, &user_service.ClientPrimaryKey{
			Id: order.ClientId,
		})
		if err != nil {
			u.log.Error("Error While Download: u.services.ClientService().GetById", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		infos.ClientName = client.Name
		infos.PhoneNumber = client.PhoneNumber
		infos.OrderUniqueId = order.OrderUniqueId

		// Get Branch Name
		branch, err := u.services.BranchService().GetById(ctx, &user_service.BranchPrimaryKey{
			Id: order.BranchId,
		})
		if err != nil {
			u.log.Error("Error While Download: u.services.BranchService().GetById", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		infos.BranchName = branch.Name

		// Get Car to Get Model and Brand
		car, err := u.services.CarService().GetById(ctx, &car_service.CarPrimaryKey{
			Id: order.CarId,
		})
		if err != nil {
			u.log.Error("Error While Download: u.services.CarService().GetById", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		// Get Model
		model, err := u.services.ModelService().GetById(ctx, &car_service.ModelPrimaryKey{
			Id: car.ModelId,
		})
		if err != nil {
			u.log.Error("Error While Download: u.services.ModelService().GetById", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		infos.Modelname = model.Name
		// Get Brand
		brand, err := u.services.BrandService().GetById(ctx, &car_service.BrandPrimaryKey{
			Id: car.BrandId,
		})
		if err != nil {
			u.log.Error("Error While Download: u.services.BrandService().GetById", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		infos.BrandName = brand.Name
		infos.StateNumber = car.StateNumber

		// Get Investor
		investor, err := u.services.InvestorService().GetById(ctx, &user_service.InvestorPrimaryKey{
			Id: car.InvestorId,
		})
		if err != nil {
			u.log.Error("Error While Download: u.services.InvestorService().GetById", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		infos.InvestorName = investor.Name
		infos.Duration = order.Duration

		// Get Tarif
		tarif, err := u.strg.Tarif().GetByID(ctx, &order_service.TarifPrimaryKey{
			Id: order.TarifId,
		})
		if err != nil {
			u.log.Error("Error While Download: u.strg.Tarif().GetByID", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		infos.TarifName = tarif.Name
		infos.Dercription = tarif.Description
		infos.TarifPrice = tarif.Price
		infos.InsuranceSum = tarif.InsurancePrice
		infos.DiscountSum = order.Discount

		// Get Order Tarif
		order_tarif, err := u.strg.OrderTarif().GetByID(ctx, &order_service.OrderTarifPrimaryKey{
			Id: order.Id,
		})
		if err != nil {
			u.log.Error("Error While Download: u.strg.OrderTarif().GetByID", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		infos.InvestorSum = order_tarif.Investor
		infos.CompanySum = order_tarif.Company
		infos.TotalSum = order_tarif.Investor + order_tarif.Company

		// Get Order Payment
		order_payments, err := u.strg.OrderPayment().GetList(ctx, &order_service.OrderPaymentGetListRequest{})
		if err != nil {
			u.log.Error("Error While Download: u.strg.OrderPayment().GetList", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		for _, order_payment := range order_payments.OrderPayments {
			if order_payment.OrderId == order.Id {
				infos.Cash = order_payment.Cash
				infos.Uzcard = order_payment.Uzcard
				infos.Humo = order_payment.Humo
				infos.Payme = order_payment.Payme
				infos.Click = order_payment.Click
			}
		}
		infos.Status = order.Paid

		Excel = append(Excel, infos)
	}

	file := excelize.NewFile()
	defer func() {
		if err := file.Close(); err != nil {
			u.log.Error(err.Error())
			return
		}
	}()

	// Create Sheet
	_, err = file.NewSheet("Sheet1")
	if err != nil {
		u.log.Error(err.Error())
		return nil, err
	}

	// Headers
	columnHeaders := []string{"Клиент", "Номер", "Заказ-Ид", "Филиал", "Бренд", "Модел",
		"Номер-Машины", "Инвестор", "Количество Дней", "Тариф", "Сумма-Тарифа", "Сумма-Страховки",
		"Сумма-Скидки", "Сумма-Инвестора", "Общий-Сумма", "Сумма-Копании", "Наличный", "Узкард",
		"Хумо", "Пайми", "Клик", "Статус", "Описание"}

	// Fill Headers
	for idx, columnHeader := range columnHeaders {
		cell, err := excelize.CoordinatesToCellName(idx+1, 1) // +1 так как индексы начинаются с 1
		if err != nil {
			return nil, err
		}
		file.SetCellValue("Sheet1", cell, columnHeader)
	}

	for idx, data := range Excel {
		rowValues := []interface{}{
			data.ClientName,
			data.PhoneNumber,
			data.OrderUniqueId,
			data.BranchName,
			data.BrandName,
			data.Modelname,
			data.StateNumber,
			data.InvestorName,
			data.Duration,
			data.TarifName,
			data.TarifPrice,
			data.InsuranceSum,
			data.DiscountSum,
			data.InvestorSum,
			data.TotalSum,
			data.CompanySum,
			data.Cash,
			data.Uzcard,
			data.Humo,
			data.Payme,
			data.Click,
			data.Status,
			data.Dercription,
		}

		for colIdx, value := range rowValues {
			cell, err := excelize.CoordinatesToCellName(colIdx+1, idx+2)
			if err != nil {
				fmt.Println(err)
				return nil, err
			}
			file.SetCellValue("Sheet1", cell, value)
		}
	}

	if err := file.SaveAs("/home/abdulbosit/go/src/gitlab.com/car_rental/order_service/uploads/Excel.xlsx"); err != nil {
		u.log.Error(err.Error())
		return nil, err
	}
	return &order_service.ExcelResponse{
		Link: "../../uploads/Excel.xlsx",
	}, nil
}
