package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/car_rental/order_service/config"
	"gitlab.com/car_rental/order_service/genproto/car_service"
	"gitlab.com/car_rental/order_service/genproto/order_service"
	"gitlab.com/car_rental/order_service/grpc/client"
	"gitlab.com/car_rental/order_service/pkg/logger"
	"gitlab.com/car_rental/order_service/storage"
)

type ReceiveCarService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedRecieveCarServiceServer
}

func NewReceiveCarService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ReceiveCarService {
	return &ReceiveCarService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *ReceiveCarService) Create(ctx context.Context, req *order_service.RecieveCarCreate) (*order_service.RecieveCar, error) {
	u.log.Info("====== RecieveCar Create ======", logger.Any("req", req))

	resp, err := u.strg.RecieveCar().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create RecieveCar: u.strg.RecieveCar().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	order, err := u.strg.Order().GetByID(ctx, &order_service.OrderPrimaryKey{Id: req.OrderId})
	if err != nil {
		u.log.Error("Error While Create RecieveCar: u.strg.Order().GetByID", logger.Error(err))
		_ = u.strg.RecieveCar().Delete(ctx, &order_service.RecieveCarPrimaryKey{Id: resp.Id})
		return nil, err
	}

	_, err = u.strg.Order().Update(ctx, &order_service.OrderUpdate{
		Id:             order.Id,
		TarifId:        order.TarifId,
		ClientId:       order.ClientId,
		BranchId:       order.BranchId,
		CarId:          order.CarId,
		Date:           order.Date,
		Duration:       order.Duration,
		ExtendDuration: order.ExtendDuration,
		Discount:       order.Discount,
		Insurance:      order.Insurance,
		Status:         "client_returned",
		OrderUniqueId:  order.OrderUniqueId,
		Paid:           order.Paid,
	})
	if err != nil {
		u.log.Error("Error While Create Recieve-Car: u.strg.Order().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	car, err := u.services.CarService().GetById(ctx, &car_service.CarPrimaryKey{Id: order.CarId})
	if err != nil {
		u.log.Error("Error While Create RecieveCar: u.services.CarService().GetById", logger.Error(err))
		_ = u.strg.RecieveCar().Delete(ctx, &order_service.RecieveCarPrimaryKey{Id: resp.Id})
		_, _ = u.strg.Order().Update(ctx, &order_service.OrderUpdate{
			Id:             order.Id,
			TarifId:        order.TarifId,
			ClientId:       order.ClientId,
			BranchId:       order.BranchId,
			CarId:          order.CarId,
			Date:           order.Date,
			Duration:       order.Duration,
			ExtendDuration: order.ExtendDuration,
			Discount:       order.Discount,
			Insurance:      order.Insurance,
			Status:         order.Status,
			OrderUniqueId:  order.OrderUniqueId,
			Paid:           order.Paid,
		})

		return nil, err
	}

	tarif, err := u.strg.Tarif().GetByID(ctx, &order_service.TarifPrimaryKey{Id: order.TarifId})
	if err != nil {
		u.log.Error("Error While Create Recieve-Car: u.strg.Tarif().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// returned.mileage - given.mileage > order.duration * daily_limit
	if req.Mileage-car.Mileage > order.Duration*tarif.DayLimit {
		_, _ = u.strg.Debt().Create(ctx, &order_service.DeptCreate{
			UserId:  order.ClientId,
			CarId:   order.CarId,
			OrderId: order.Id,
			Amount:  ((req.Mileage - car.Mileage) - (order.Duration * tarif.DayLimit)) * tarif.OverLimit,
			Paid:    false,
		})
	}
	return resp, nil
}

func (u *ReceiveCarService) GetById(ctx context.Context, req *order_service.RecieveCarPrimaryKey) (*order_service.RecieveCar, error) {
	u.log.Info("====== RecieveCar Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.RecieveCar().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While RecieveCar Get By ID: u.strg.RecieveCar().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ReceiveCarService) GetList(ctx context.Context, req *order_service.RecieveCarGetListRequest) (*order_service.RecieveCarGetListResponse, error) {
	u.log.Info("====== RecieveCar Get List ======", logger.Any("req", req))

	resp, err := u.strg.RecieveCar().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While RecieveCar Get List: u.strg.RecieveCar().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ReceiveCarService) Update(ctx context.Context, req *order_service.RecieveCarUpdate) (*order_service.RecieveCar, error) {
	u.log.Info("====== RecieveCar Update ======", logger.Any("req", req))

	resp, err := u.strg.RecieveCar().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While RecieveCar Update: u.strg.RecieveCar().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ReceiveCarService) Delete(ctx context.Context, req *order_service.RecieveCarPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== RecieveCar Delete ======", logger.Any("req", req))

	err := u.strg.RecieveCar().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While RecieveCar Delete: u.strg.RecieveCar().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
