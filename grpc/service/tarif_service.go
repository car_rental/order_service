package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/car_rental/order_service/config"
	"gitlab.com/car_rental/order_service/genproto/order_service"
	"gitlab.com/car_rental/order_service/grpc/client"
	"gitlab.com/car_rental/order_service/pkg/logger"
	"gitlab.com/car_rental/order_service/storage"
)

type TarifService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedTarifServiceServer
}

func NewTarifService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *TarifService {
	return &TarifService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *TarifService) Create(ctx context.Context, req *order_service.TarifCreate) (*order_service.Tarif, error) {
	u.log.Info("====== Tarif Create ======", logger.Any("req", req))

	resp, err := u.strg.Tarif().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Tarif: u.strg.Tarif().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *TarifService) GetById(ctx context.Context, req *order_service.TarifPrimaryKey) (*order_service.Tarif, error) {
	u.log.Info("====== Tarif Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Tarif().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Tarif Get By ID: u.strg.Tarif().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *TarifService) GetList(ctx context.Context, req *order_service.TarifGetListRequest) (*order_service.TarifGetListResponse, error) {
	u.log.Info("====== Tarif Get List ======", logger.Any("req", req))

	resp, err := u.strg.Tarif().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Tarif Get List: u.strg.Tarif().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *TarifService) Update(ctx context.Context, req *order_service.TarifUpdate) (*order_service.Tarif, error) {
	u.log.Info("====== Tarif Update ======", logger.Any("req", req))

	resp, err := u.strg.Tarif().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Tarif Update: u.strg.Tarif().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *TarifService) Delete(ctx context.Context, req *order_service.TarifPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Tarif Delete ======", logger.Any("req", req))

	err := u.strg.Tarif().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Tarif Delete: u.strg.Tarif().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
