package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/car_rental/order_service/config"
	"gitlab.com/car_rental/order_service/genproto/car_service"
	"gitlab.com/car_rental/order_service/genproto/order_service"
	"gitlab.com/car_rental/order_service/genproto/user_service"
	"gitlab.com/car_rental/order_service/grpc/client"
	"gitlab.com/car_rental/order_service/pkg/logger"
	"gitlab.com/car_rental/order_service/storage"
)

type OrderPaymentService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedOrderPaymentServiceServer
}

func NewOrderPaymentService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *OrderPaymentService {
	return &OrderPaymentService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *OrderPaymentService) Create(ctx context.Context, req *order_service.OrderPaymentCreate) (*order_service.OrderPayment, error) {
	u.log.Info("====== OrderPayment Create ======", logger.Any("req", req))

	// Get Order by Id to calculate
	order_info, err := u.strg.Order().GetByID(ctx, &order_service.OrderPrimaryKey{
		Id: req.OrderId,
	})
	if err != nil {
		u.log.Error("Error While Create OrderPayment: u.strg.Order().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// Get Car Info to Get Investor
	car, err := u.services.CarService().GetById(ctx, &car_service.CarPrimaryKey{
		Id: order_info.CarId,
	})
	if err != nil {
		u.log.Error("Error While Create OrderPayment: u.services.CarService().GetById", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// Get Investor info To Calculate
	investor, err := u.services.InvestorService().GetById(ctx, &user_service.InvestorPrimaryKey{
		Id: car.InvestorId,
	})
	if err != nil {
		u.log.Error("Error While Create OrderPayment: u.services.InvestorService().GetById", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// Get Tarif info To Calculate
	tarif, err := u.strg.Tarif().GetByID(ctx, &order_service.TarifPrimaryKey{
		Id: order_info.TarifId,
	})
	if err != nil {
		u.log.Error("Error While Create OrderPayment: u.strg.Tarif().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// Here calculated Sum of Investor And Company
	var paid_sum = req.Cash + req.Click + req.Humo + req.Payme + req.Uzcard
	var total_sum = tarif.Price * order_info.Duration
	var invesotor_sum int64
	var company_sum int64

	// Here Calculated Discount
	if order_info.Discount > 0 {
		total_sum -= order_info.Discount
	}

	// Here Calculated Investor and Company sum
	if order_info.Insurance {
		// Here Calculated Investor Sum
		invesotor_sum = ((total_sum + tarif.InsurancePrice) * investor.Percent) / 100
		// Here Calculated Company Sum
		company_sum = (total_sum - invesotor_sum)
	} else if !order_info.Insurance {
		invesotor_sum = ((total_sum + tarif.InsurancePrice) * investor.Percent) / 100
		company_sum = (total_sum - invesotor_sum - tarif.InsurancePrice)
	}

	// Here Compared paid-sum and total-sum to set status
	if paid_sum >= total_sum {
		// Here Updated Order Status
		_, err = u.strg.Order().Update(ctx, &order_service.OrderUpdate{
			Id:             order_info.Id,
			TarifId:        order_info.TarifId,
			ClientId:       order_info.ClientId,
			BranchId:       order_info.BranchId,
			CarId:          order_info.CarId,
			Date:           order_info.Date,
			Duration:       order_info.Duration,
			ExtendDuration: order_info.ExtendDuration,
			Discount:       order_info.Discount,
			Insurance:      order_info.Insurance,
			Status:         order_info.Status,
			OrderUniqueId:  order_info.OrderUniqueId,
			Paid:           "Fully",
		})
		if err != nil {
			u.log.Error("Error While Create OrderPayment: u.strg.Order().Update", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
	} else {
		// Here Updated Order Status
		_, err = u.strg.Order().Update(ctx, &order_service.OrderUpdate{
			Id:             order_info.Id,
			TarifId:        order_info.TarifId,
			ClientId:       order_info.ClientId,
			BranchId:       order_info.BranchId,
			CarId:          order_info.CarId,
			Date:           order_info.Date,
			Duration:       order_info.Duration,
			ExtendDuration: order_info.ExtendDuration,
			Discount:       order_info.Discount,
			Insurance:      order_info.Insurance,
			Status:         order_info.Status,
			OrderUniqueId:  order_info.OrderUniqueId,
			Paid:           "Not Fully",
		})
		if err != nil {
			u.log.Error("Error While Create OrderPayment: u.strg.Order().Update", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		// Here Create Debt Because he is not fully paid Service
		_, err = u.strg.Debt().Create(ctx, &order_service.DeptCreate{
			UserId:  order_info.ClientId,
			CarId:   order_info.CarId,
			OrderId: req.OrderId,
			Amount:  (tarif.Price * order_info.Duration) - paid_sum,
			Paid:    false,
		})
		if err != nil {
			u.log.Error("Error While Create OrderPayment: u.strg.Debt().Create", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
	}

	// Here Created Order_Tarif
	_, err = u.strg.OrderTarif().Create(ctx, &order_service.OrderTarifCreate{
		OrderId:       req.OrderId,
		TariffPayment: tarif.Price,
		Investor:      invesotor_sum,
		Company:       company_sum,
		Day:           order_info.Duration,
		Insurance:     tarif.InsurancePrice,
	})
	if err != nil {
		u.log.Error("Error While Create OrderPayment: u.strg.OrderTarif().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// Here Created Order Payment
	resp, err := u.strg.OrderPayment().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create OrderPayment: u.strg.OrderPayment().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderPaymentService) GetById(ctx context.Context, req *order_service.OrderPaymentPrimaryKey) (*order_service.OrderPayment, error) {
	u.log.Info("====== OrderPayment Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.OrderPayment().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While OrderPayment Get By ID: u.strg.OrderPayment().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderPaymentService) GetList(ctx context.Context, req *order_service.OrderPaymentGetListRequest) (*order_service.OrderPaymentGetListResponse, error) {
	u.log.Info("====== OrderPayment Get List ======", logger.Any("req", req))

	resp, err := u.strg.OrderPayment().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While OrderPayment Get List: u.strg.OrderPayment().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderPaymentService) Update(ctx context.Context, req *order_service.OrderPaymentUpdate) (*order_service.OrderPayment, error) {
	u.log.Info("====== OrderPayment Update ======", logger.Any("req", req))

	resp, err := u.strg.OrderPayment().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While OrderPayment Update: u.strg.OrderPayment().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *OrderPaymentService) Delete(ctx context.Context, req *order_service.OrderPaymentPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== OrderPayment Delete ======", logger.Any("req", req))

	err := u.strg.OrderPayment().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While OrderPayment Delete: u.strg.OrderPayment().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
