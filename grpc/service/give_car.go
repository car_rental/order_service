package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/car_rental/order_service/config"
	"gitlab.com/car_rental/order_service/genproto/car_service"
	"gitlab.com/car_rental/order_service/genproto/order_service"
	"gitlab.com/car_rental/order_service/grpc/client"
	"gitlab.com/car_rental/order_service/pkg/logger"
	"gitlab.com/car_rental/order_service/storage"
)

type GiveCarService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedGiveCarServiceServer
}

func NewGiveCarService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *GiveCarService {
	return &GiveCarService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *GiveCarService) Create(ctx context.Context, req *order_service.GiveCarCreate) (*order_service.GiveCar, error) {
	u.log.Info("====== GiveCar Create ======", logger.Any("req", req))

	resp, err := u.strg.GiveCar().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create GiveCar: u.strg.GiveCar().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	order, err := u.strg.Order().GetByID(ctx, &order_service.OrderPrimaryKey{Id: req.OrderId})
	if err != nil {
		u.log.Error("Error While Create GiveCar: u.strg.Order().GetByID", logger.Error(err))
		_ = u.strg.GiveCar().Delete(ctx, &order_service.GiveCarPrimaryKey{Id: resp.Id})
		return nil, err
	}

	_, err = u.strg.Order().Update(ctx, &order_service.OrderUpdate{
		Id:             order.Id,
		TarifId:        order.TarifId,
		ClientId:       order.ClientId,
		BranchId:       order.BranchId,
		CarId:          order.CarId,
		Date:           order.Date,
		Duration:       order.Duration,
		ExtendDuration: order.ExtendDuration,
		Discount:       order.Discount,
		Insurance:      order.Insurance,
		Status:         "client_took",
		OrderUniqueId:  order.OrderUniqueId,
		Paid:           order.Paid,
	})
	if err != nil {
		u.log.Error("Error While Create GiveCar: u.strg.Order().Update", logger.Error(err))
		_ = u.strg.GiveCar().Delete(ctx, &order_service.GiveCarPrimaryKey{Id: resp.Id})
		return nil, err
	}
	car, err := u.services.CarService().GetById(ctx, &car_service.CarPrimaryKey{Id: order.CarId})
	if err != nil {
		u.log.Error("Error While Create GiveCar: u.services.CarService().GetById", logger.Error(err))
		_ = u.strg.GiveCar().Delete(ctx, &order_service.GiveCarPrimaryKey{Id: resp.Id})
		_, _ = u.strg.Order().Update(ctx, &order_service.OrderUpdate{
			Id:             order.Id,
			TarifId:        order.TarifId,
			ClientId:       order.ClientId,
			BranchId:       order.BranchId,
			CarId:          order.CarId,
			Date:           order.Date,
			Duration:       order.Duration,
			ExtendDuration: order.ExtendDuration,
			Discount:       order.Discount,
			Insurance:      order.Insurance,
			Status:         order.Status,
			OrderUniqueId:  order.OrderUniqueId,
			Paid:           order.Paid,
		})

		return nil, err
	}

	_, err = u.services.CarService().Update(ctx, &car_service.CarUpdate{
		Id:          car.Id,
		BrandId:     car.BrandId,
		ModelId:     car.ModelId,
		StateNumber: car.StateNumber,
		Mileage:     req.Mileage,
		InvestorId:  car.InvestorId,
		Status:      "in_use",
	})

	if err != nil {
		u.log.Error("Error While Create GiveCar: u.services.CarService().Update", logger.Error(err))
		_ = u.strg.GiveCar().Delete(ctx, &order_service.GiveCarPrimaryKey{Id: resp.Id})
		_, _ = u.strg.Order().Update(ctx, &order_service.OrderUpdate{
			Id:             order.Id,
			TarifId:        order.TarifId,
			ClientId:       order.ClientId,
			BranchId:       order.BranchId,
			CarId:          order.CarId,
			Date:           order.Date,
			Duration:       order.Duration,
			ExtendDuration: order.ExtendDuration,
			Discount:       order.Discount,
			Insurance:      order.Insurance,
			Status:         order.Status,
			OrderUniqueId:  order.OrderUniqueId,
			Paid:           order.Paid,
		})
		return nil, err
	}

	return resp, nil
}

func (u *GiveCarService) GetById(ctx context.Context, req *order_service.GiveCarPrimaryKey) (*order_service.GiveCar, error) {
	u.log.Info("====== GiveCar Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.GiveCar().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While GiveCar Get By ID: u.strg.GiveCar().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *GiveCarService) GetList(ctx context.Context, req *order_service.GiveCarGetListRequest) (*order_service.GiveCarGetListResponse, error) {
	u.log.Info("====== GiveCar Get List ======", logger.Any("req", req))

	resp, err := u.strg.GiveCar().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While GiveCar Get List: u.strg.GiveCar().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *GiveCarService) Update(ctx context.Context, req *order_service.GiveCarUpdate) (*order_service.GiveCar, error) {
	u.log.Info("====== GiveCar Update ======", logger.Any("req", req))

	resp, err := u.strg.GiveCar().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While GiveCar Update: u.strg.GiveCar().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *GiveCarService) Delete(ctx context.Context, req *order_service.GiveCarPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== GiveCar Delete ======", logger.Any("req", req))

	err := u.strg.GiveCar().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While GiveCar Delete: u.strg.GiveCar().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
