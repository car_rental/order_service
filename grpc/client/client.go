package client

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/car_rental/order_service/config"
	"gitlab.com/car_rental/order_service/genproto/car_service"
	"gitlab.com/car_rental/order_service/genproto/user_service"
)

type ServiceManagerI interface {
	// User Service
	ClientService() user_service.ClientServiceClient
	InvestorService() user_service.InvestorServiceClient
	MechanicService() user_service.MechanicServiceClient
	BranchService() user_service.BranchServiceClient

	// Car Service
	CarService() car_service.CarServiceClient
	ModelService() car_service.ModelServiceClient
	BrandService() car_service.BrandServiceClient
}

type grpcClients struct {
	// User Service
	clientService   user_service.ClientServiceClient
	investorService user_service.InvestorServiceClient
	mechanicService user_service.MechanicServiceClient
	branchService   user_service.BranchServiceClient

	// Car Service
	carService   car_service.CarServiceClient
	modelService car_service.ModelServiceClient
	brandService car_service.BrandServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// User Microservice
	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	// Car Microservice
	connCarService, err := grpc.Dial(
		cfg.CarServiceHost+cfg.CarGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}
	return &grpcClients{
		// User Service
		clientService:   user_service.NewClientServiceClient(connUserService),
		investorService: user_service.NewInvestorServiceClient(connUserService),
		mechanicService: user_service.NewMechanicServiceClient(connUserService),
		branchService:   user_service.NewBranchServiceClient(connUserService),

		// Car Service
		carService:   car_service.NewCarServiceClient(connCarService),
		modelService: car_service.NewModelServiceClient(connCarService),
		brandService: car_service.NewBrandServiceClient(connCarService),
	}, nil
}

// User Service

func (g *grpcClients) ClientService() user_service.ClientServiceClient {
	return g.clientService
}

func (g *grpcClients) InvestorService() user_service.InvestorServiceClient {
	return g.investorService
}

func (g *grpcClients) MechanicService() user_service.MechanicServiceClient {
	return g.mechanicService
}
func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.branchService
}

// Car Service

func (g *grpcClients) CarService() car_service.CarServiceClient {
	return g.carService
}

func (g *grpcClients) ModelService() car_service.ModelServiceClient {
	return g.modelService
}
func (g *grpcClients) BrandService() car_service.BrandServiceClient {
	return g.brandService
}
