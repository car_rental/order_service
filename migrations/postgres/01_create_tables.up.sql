CREATE TABLE tarif (
    id UUID PRIMARY KEY not NULL,
    photo varchar,
    model_id UUID NOT NULL,
    name varchar not NULL,
    price int not NULL,
    day_limit int not NULL,
    over_limit int,
    insurance_price int not null,
    "description" varchar,
    status boolean,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);
CREATE TABLE "order" (
    id UUID PRIMARY KEY not NULL,
    tarif_id uuid REFERENCES tarif("id"),
    client_id uuid NOT NULL,
    branch_id uuid not null,
    car_id uuid not null,
    "date" date,
    duration int not null,
    extend_duration boolean DEFAULT false,
    discount int,
    insurance boolean DEFAULT false,
    "status" varchar DEFAULT 'new',
    order_unique_id varchar UNIQUE,
    paid varchar,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE order_payment(
    id UUID PRIMARY KEY not null,
    order_id UUID REFERENCES "order"("id"),
    cash int,
    humo int,
    uzcard int,
    click int,
    payme int,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE give_car (
    id UUID PRIMARY KEY not null,
    order_id uuid REFERENCES "order"("id"),
    mechanik_id uuid NOT NULL,
    mileage int not null,
    gas int not null,
    photo varchar not null, 
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE recieve_car (
    id UUID PRIMARY KEY not null,
    order_id uuid REFERENCES "order"("id"),
    mechanik_id uuid NOT NULL,
    mileage int not null,
    gas int not null,
    photo varchar not null, 
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);