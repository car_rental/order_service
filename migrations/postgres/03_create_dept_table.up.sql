CREATE TABLE dept(
    id UUID PRIMARY KEY NOT NULL ,
    order_id UUID REFERENCES "order"("id"),
    user_id UUID NOT NULL,
    car_id UUID NOT NULL,
    debt INT NOT NULL,
    paid BOOLEAN DEFAULT FALSE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);
