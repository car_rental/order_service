CREATE TABLE order_tarif (
    "id" UUID PRIMARY KEY,
    "order_id" UUID REFERENCES "order"("id"),
    "tariff_payment" int not null,
    "investor" int not null,
    "insurance" int,
    "company" int,
    "day" int,
    "status" varchar,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
)