package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"math/rand"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/order_service/genproto/order_service"
	"gitlab.com/car_rental/order_service/pkg/helper"
)

type OrderRepo struct {
	db *pgxpool.Pool
}

func NewOrderRepo(db *pgxpool.Pool) *OrderRepo {
	return &OrderRepo{
		db: db,
	}
}

func (r *OrderRepo) Create(ctx context.Context, req *order_service.OrderCreate) (*order_service.Order, error) {

	var (
		id              = uuid.New().String()
		query           string
		order_unique_id string
	)

	for i := 0; len(order_unique_id) < 9; i++ {
		random_number := rand.Intn(10) // Generate a random number between 0 and 9
		order_unique_id += fmt.Sprintf("%d", random_number)
	}

	query = `
		INSERT INTO "order"(id, tarif_id, client_id, branch_id, car_id, date, 
			duration, discount, insurance, order_unique_id, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		helper.NewNullString(req.TarifId),
		helper.NewNullString(req.ClientId),
		helper.NewNullString(req.BranchId),
		helper.NewNullString(req.CarId),
		req.Date,
		req.Duration,
		req.Discount,
		req.Insurance,
		order_unique_id,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.Order{
		Id:            id,
		TarifId:       req.TarifId,
		ClientId:      req.ClientId,
		BranchId:      req.BranchId,
		CarId:         req.CarId,
		Date:          req.Date,
		Duration:      req.Duration,
		Discount:      req.Discount,
		OrderUniqueId: order_unique_id,
	}, nil
}

func (r *OrderRepo) GetByID(ctx context.Context, req *order_service.OrderPrimaryKey) (*order_service.Order, error) {

	// var whereField = "id"
	// if len(req.Username) > 0 {
	// 	whereField = "username"
	// 	req.Id = req.Username
	// }

	// Id             string
	// TarifId        string
	// ClientId       string
	// BranchId       string
	// CarId          string
	// Date           string
	// Duration       int64
	// ExtendDuration bool
	// Discount       int64
	// Insurance      bool
	// Status         string
	// OrderUniqueId  string
	// Paid           bool
	// CreatedAt      string
	// UpdatedAt      string

	var (
		query string

		Id             sql.NullString
		TarifId        sql.NullString
		ClientId       sql.NullString
		BranchId       sql.NullString
		CarId          sql.NullString
		Date           sql.NullString
		Duration       sql.NullInt64
		ExtendDuration sql.NullBool
		Discount       sql.NullInt64
		Insurance      sql.NullBool
		Status         sql.NullString
		OrderUniqueId  sql.NullString
		Paid           sql.NullString
		CreatedAt      sql.NullString
		UpdatedAt      sql.NullString
	)

	query = `
		SELECT
			id,
			tarif_id,
			client_id,
			branch_id,
			car_id,
			date,
			duration,
			extend_duration,
			discount,
			insurance,
			status,
			order_unique_id,
			paid,
			created_at,
			updated_at		
		FROM "order"
		WHERE id = $1
	`
	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&TarifId,
		&ClientId,
		&BranchId,
		&CarId,
		&Date,
		&Duration,
		&ExtendDuration,
		&Discount,
		&Insurance,
		&Status,
		&OrderUniqueId,
		&Paid,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.Order{
		Id:             Id.String,
		TarifId:        TarifId.String,
		ClientId:       ClientId.String,
		BranchId:       BranchId.String,
		CarId:          CarId.String,
		Date:           Date.String,
		Duration:       Duration.Int64,
		ExtendDuration: ExtendDuration.Bool,
		Discount:       Discount.Int64,
		Insurance:      Insurance.Bool,
		Status:         Status.String,
		OrderUniqueId:  OrderUniqueId.String,
		Paid:           Paid.String,
		CreatedAt:      CreatedAt.String,
		UpdatedAt:      UpdatedAt.String,
	}, nil
}

func (r *OrderRepo) GetList(ctx context.Context, req *order_service.OrderGetListRequest) (*order_service.OrderGetListResponse, error) {

	var (
		resp   = &order_service.OrderGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			tarif_id,
			client_id,
			branch_id,
			car_id,
			date,
			duration,
			extend_duration,
			discount,
			insurance,
			status,
			order_unique_id,
			paid,
			created_at,
			updated_at		
		FROM "order"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByDate != "" {
		where += ` AND data ILIKE '%' || '` + req.SearchByDate + `' || '%'`
	}

	if req.SearchByOrderUniqueId != "" {
		where += ` AND order_unique_id ILIKE '%' || '` + req.SearchByOrderUniqueId + `' || '%'`
	}

	if req.SearchByTarif != "" {
		where += ` AND tarif_id ILIKE '%' || '` + req.SearchByTarif + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id             sql.NullString
			TarifId        sql.NullString
			ClientId       sql.NullString
			BranchId       sql.NullString
			CarId          sql.NullString
			Date           sql.NullString
			Duration       sql.NullInt64
			ExtendDuration sql.NullBool
			Discount       sql.NullInt64
			Insurance      sql.NullBool
			Status         sql.NullString
			OrderUniqueId  sql.NullString
			Paid           sql.NullString
			CreatedAt      sql.NullString
			UpdatedAt      sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&TarifId,
			&ClientId,
			&BranchId,
			&CarId,
			&Date,
			&Duration,
			&ExtendDuration,
			&Discount,
			&Insurance,
			&Status,
			&OrderUniqueId,
			&Paid,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Orders = append(resp.Orders, &order_service.Order{
			Id:             Id.String,
			TarifId:        TarifId.String,
			ClientId:       ClientId.String,
			BranchId:       BranchId.String,
			CarId:          CarId.String,
			Date:           Date.String,
			Duration:       Duration.Int64,
			ExtendDuration: ExtendDuration.Bool,
			Discount:       Discount.Int64,
			Insurance:      Insurance.Bool,
			Status:         Status.String,
			OrderUniqueId:  OrderUniqueId.String,
			Paid:           Paid.String,
			CreatedAt:      CreatedAt.String,
			UpdatedAt:      UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *OrderRepo) Update(ctx context.Context, req *order_service.OrderUpdate) (*order_service.Order, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"order"
		SET
			tarif_id = :TarifId,
			client_id = :ClientId,
			branch_id = :BranchId,
			car_id = :CarId,
			date = :Date,
			duration = :Duration,
			extend_duration = :ExtendDuration,
			discount = :Discount,
			insurance = :Insurance,
			status = :Status,
			order_unique_id = :OrderUniqueId,
			paid = :Paid,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":             req.Id,
		"TarifId":        helper.NewNullString(req.TarifId),
		"ClientId":       helper.NewNullString(req.ClientId),
		"BranchId":       helper.NewNullString(req.BranchId),
		"CarId":          helper.NewNullString(req.CarId),
		"Date":           req.Date,
		"Duration":       req.Duration,
		"ExtendDuration": req.ExtendDuration,
		"Discount":       req.Discount,
		"Insurance":      req.Insurance,
		"Status":         req.Status,
		"OrderUniqueId":  req.OrderUniqueId,
		"Paid":           req.Paid,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &order_service.Order{
		Id:             req.Id,
		TarifId:        req.TarifId,
		ClientId:       req.ClientId,
		BranchId:       req.BranchId,
		CarId:          req.CarId,
		Date:           req.Date,
		Duration:       req.Duration,
		ExtendDuration: req.ExtendDuration,
		Discount:       req.Discount,
		Insurance:      req.Insurance,
		Status:         req.Status,
		OrderUniqueId:  req.OrderUniqueId,
		Paid:           req.Paid,
	}, nil
}

func (r *OrderRepo) Delete(ctx context.Context, req *order_service.OrderPrimaryKey) error {

	_, err := r.db.Exec(ctx, `DELETE FROM "order" WHERE id = $1`, req.Id)
	if err != nil {
		return err
	}

	return nil
}
