package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/order_service/genproto/order_service"
	"gitlab.com/car_rental/order_service/pkg/helper"
)

type TarifRepo struct {
	db *pgxpool.Pool
}

func NewTarifRepo(db *pgxpool.Pool) *TarifRepo {
	return &TarifRepo{
		db: db,
	}
}

func (r *TarifRepo) Create(ctx context.Context, req *order_service.TarifCreate) (*order_service.Tarif, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "tarif"(id, photo, model_id, name, price, day_limit, 
			over_limit, insurance_price, description, status, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Photo,
		req.ModelId,
		req.Name,
		req.Price,
		req.DayLimit,
		req.OverLimit,
		req.InsurancePrice,
		req.Description,
		req.Status,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.Tarif{
		Id:             id,
		Photo:          req.Photo,
		ModelId:        req.ModelId,
		Name:           req.Name,
		Price:          req.Price,
		DayLimit:       req.DayLimit,
		OverLimit:      req.OverLimit,
		InsurancePrice: req.InsurancePrice,
		Description:    req.Description,
		Status:         req.Status,
	}, nil
}

func (r *TarifRepo) GetByID(ctx context.Context, req *order_service.TarifPrimaryKey) (*order_service.Tarif, error) {

	// var whereField = "id"
	// if len(req.Username) > 0 {
	// 	whereField = "username"
	// 	req.Id = req.Username
	// }

	// id UUID PRIMARY KEY not NULL,
	// photo varchar,
	// model_id UUID NOT NULL,
	// name varchar not NULL,
	// price int not NULL,
	// day_limit int not NULL,
	// over_limit int,
	// insurance_price int not null,
	// description varchar,
	// status boolean,

	var (
		query string

		id              sql.NullString
		photo           sql.NullString
		model_id        sql.NullString
		name            sql.NullString
		price           sql.NullInt64
		day_limit       sql.NullInt64
		over_limit      sql.NullInt64
		insurance_price sql.NullInt64
		description     sql.NullString
		status          sql.NullBool
		created_at      sql.NullString
		updated_at      sql.NullString
	)

	query = `
		SELECT
			id,
			photo,
			model_id,
			name,
			price,
			day_limit,
			over_limit,
			insurance_price,
			description,
			status,
			created_at,
			updated_at		
		FROM "tarif"
		WHERE id = $1
	`
	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&photo,
		&model_id,
		&name,
		&price,
		&day_limit,
		&over_limit,
		&insurance_price,
		&description,
		&status,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.Tarif{
		Id:             id.String,
		Photo:          photo.String,
		ModelId:        model_id.String,
		Name:           name.String,
		Price:          price.Int64,
		DayLimit:       day_limit.Int64,
		OverLimit:      over_limit.Int64,
		InsurancePrice: insurance_price.Int64,
		Description:    description.String,
		Status:         status.Bool,
		CreatedAt:      created_at.String,
		UpdatedAt:      updated_at.String,
	}, nil
}

func (r *TarifRepo) GetList(ctx context.Context, req *order_service.TarifGetListRequest) (*order_service.TarifGetListResponse, error) {

	var (
		resp   = &order_service.TarifGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			photo,
			model_id,
			name,
			price,
			day_limit,
			over_limit,
			insurance_price,
			description,
			status,
			created_at,
			updated_at		
		FROM "tarif"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id              sql.NullString
			photo           sql.NullString
			model_id        sql.NullString
			name            sql.NullString
			price           sql.NullInt64
			day_limit       sql.NullInt64
			over_limit      sql.NullInt64
			insurance_price sql.NullInt64
			description     sql.NullString
			status          sql.NullBool
			created_at      sql.NullString
			updated_at      sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&photo,
			&model_id,
			&name,
			&price,
			&day_limit,
			&over_limit,
			&insurance_price,
			&description,
			&status,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Tarifs = append(resp.Tarifs, &order_service.Tarif{
			Id:             id.String,
			Photo:          photo.String,
			ModelId:        model_id.String,
			Name:           name.String,
			Price:          price.Int64,
			DayLimit:       day_limit.Int64,
			OverLimit:      over_limit.Int64,
			InsurancePrice: insurance_price.Int64,
			Description:    description.String,
			Status:         status.Bool,
			CreatedAt:      created_at.String,
			UpdatedAt:      updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *TarifRepo) Update(ctx context.Context, req *order_service.TarifUpdate) (*order_service.Tarif, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"tarif"
		SET
			photo = :photo,
			model_id = :model_id,
			name = :name,
			price = :price,
			day_limit = :day_limit,
			over_limit = :over_limit,
			insurance_price = :insurance_price,
			description = :description,
			status = :status,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":              req.Id,
		"photo":           req.Photo,
		"model_id":        req.ModelId,
		"name":            req.Name,
		"price":           req.Price,
		"day_limit":       req.DayLimit,
		"over_limit":      req.OverLimit,
		"insurance_price": req.InsurancePrice,
		"description":     req.Description,
		"status":          req.Status,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &order_service.Tarif{
		Id:             req.Id,
		Photo:          req.Photo,
		ModelId:        req.ModelId,
		Name:           req.Name,
		Price:          req.Price,
		DayLimit:       req.DayLimit,
		OverLimit:      req.OverLimit,
		InsurancePrice: req.InsurancePrice,
		Description:    req.Description,
		Status:         req.Status,
	}, nil
}

func (r *TarifRepo) Delete(ctx context.Context, req *order_service.TarifPrimaryKey) error {

	_, err := r.db.Exec(ctx, `DELETE FROM "tarif" WHERE id = $1`, req.Id)
	if err != nil {
		return err
	}

	return nil
}
