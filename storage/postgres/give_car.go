package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/order_service/genproto/order_service"
	"gitlab.com/car_rental/order_service/pkg/helper"
)

type GiveCarRepo struct {
	db *pgxpool.Pool
}

func NewGiveCarRepo(db *pgxpool.Pool) *GiveCarRepo {
	return &GiveCarRepo{
		db: db,
	}
}
func (r *GiveCarRepo) Create(ctx context.Context, req *order_service.GiveCarCreate) (*order_service.GiveCar, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "give_car"(
			id, 
			order_id,
			mechanik_id,
		    mileage,
		    gas, 
		   photo, 
		   updated_at
		)
		VALUES ($1, $2, $3, $4, $5, $6,NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		helper.NewNullString(req.OrderId),
		helper.NewNullString(req.MechanicId),
		req.Mileage,
		req.Gas,
		req.Photo,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.GiveCar{
		Id:         id,
		OrderId:    req.OrderId,
		MechanicId: req.MechanicId,
		Mileage:    req.Mileage,
		Gas:        req.Gas,
		Photo:      req.Photo,
	}, nil
}

func (r *GiveCarRepo) GetByID(ctx context.Context, req *order_service.GiveCarPrimaryKey) (*order_service.GiveCar, error) {

	// var whereField = "id"
	// if len(req.Username) > 0 {
	// 	whereField = "username"
	// 	req.Id = req.Username
	// }

	var (
		query string

		id          sql.NullString
		order_id    sql.NullString
		mechanik_id sql.NullString
		mileage     sql.NullInt64
		gas         sql.NullInt64
		photo       sql.NullString
		created_at  sql.NullString
		updated_at  sql.NullString
	)

	query = `
		SELECT
		id, 
		order_id,
		mechanik_id,
		mileage,
		gas, 
	    photo, 
		created_at,
		updated_at		
		FROM "give_car"
		WHERE id = $1
	`
	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&order_id,
		&mechanik_id,
		&mileage,
		&gas,
		&photo,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.GiveCar{
		Id:         id.String,
		OrderId:    order_id.String,
		MechanicId: mechanik_id.String,
		Mileage:    mileage.Int64,
		Gas:        gas.Int64,
		Photo:      photo.String,
		CreatedAt:  created_at.String,
		UpdatedAt:  updated_at.String,
	}, nil
}

func (r *GiveCarRepo) GetList(ctx context.Context, req *order_service.GiveCarGetListRequest) (*order_service.GiveCarGetListResponse, error) {

	var (
		resp   = &order_service.GiveCarGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id, 
			order_id,
			mechanik_id,
			mileage,
			gas, 
	    	photo, 
			created_at,
			updated_at		
			FROM "give_car"
		`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND mileage ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			order_id    sql.NullString
			mechanik_id sql.NullString
			mileage     sql.NullInt64
			gas         sql.NullInt64
			photo       sql.NullString
			created_at  sql.NullString
			updated_at  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&order_id,
			&mechanik_id,
			&mileage,
			&gas,
			&photo,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.GiveCars = append(resp.GiveCars, &order_service.GiveCar{
			Id:         id.String,
			OrderId:    order_id.String,
			MechanicId: mechanik_id.String,
			Mileage:    mileage.Int64,
			Gas:        gas.Int64,
			Photo:      photo.String,
			CreatedAt:  created_at.String,
			UpdatedAt:  updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *GiveCarRepo) Update(ctx context.Context, req *order_service.GiveCarUpdate) (*order_service.GiveCar, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"give_car"
		SET
			order_id = :order_id,
			mechanik_id = :mechanik_id,
			mileage = :mileage,
			gas = :gas,
			photo = :photo,
			updated_at = NOW()
		WHERE id = :id
	`
	params = map[string]interface{}{
		"id":          req.Id,
		"order_id":    helper.NewNullString(req.OrderId),
		"mechanik_id": helper.NewNullString(req.MechanicId),
		"mileage":     req.Mileage,
		"gas":         req.Gas,
		"photo":       req.Photo,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &order_service.GiveCar{
		Id:         req.Id,
		OrderId:    req.OrderId,
		MechanicId: req.MechanicId,
		Mileage:    req.Mileage,
		Gas:        req.Gas,
		Photo:      req.Photo,
	}, nil
}

func (r *GiveCarRepo) Delete(ctx context.Context, req *order_service.GiveCarPrimaryKey) error {

	_, err := r.db.Exec(ctx, `DELETE FROM "give_car" WHERE id = $1`, req.Id)
	if err != nil {
		return err
	}

	return nil
}
