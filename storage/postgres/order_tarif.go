package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/order_service/genproto/order_service"
	"gitlab.com/car_rental/order_service/pkg/helper"
)

type OrderTarifRepo struct {
	db *pgxpool.Pool
}

func NewOrderTarifRepo(db *pgxpool.Pool) *OrderTarifRepo {
	return &OrderTarifRepo{
		db: db,
	}
}

func (r *OrderTarifRepo) Create(ctx context.Context, req *order_service.OrderTarifCreate) (*order_service.OrderTarif, error) {
	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO order_tarif(id, order_id,  tariff_payment, investor, insurance, company,
			day, status, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8 , NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		helper.NewNullString(req.OrderId),
		req.TariffPayment,
		req.Investor,
		req.Insurance,
		req.Company,
		req.Day,
		req.Status,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.OrderTarif{
		Id:            id,
		TariffPayment: req.TariffPayment,
	}, nil
}
func (r *OrderTarifRepo) GetByID(ctx context.Context, req *order_service.OrderTarifPrimaryKey) (*order_service.OrderTarif, error) {
	var (
		query string

		id             sql.NullString
		order_id       sql.NullString
		tariff_payment sql.NullInt64
		investor       sql.NullInt64
		insurance      sql.NullInt64
		company        sql.NullInt64
		day            sql.NullInt64
		status         sql.NullString
		created_at     sql.NullString
		updated_at     sql.NullString
	)

	query = `
		SELECT
			id,
			order_id,
			tariff_payment,
			investor,
			insurance,
			company,
			day,
			status,
			created_at,
			updated_at		
		FROM "order_tarif"
		WHERE order_id = $1
	`
	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&order_id,
		&tariff_payment,
		&investor,
		&insurance,
		&company,
		&day,
		&status,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.OrderTarif{
		Id:            id.String,
		OrderId:       order_id.String,
		TariffPayment: tariff_payment.Int64,
		Investor:      investor.Int64,
		Insurance:     insurance.Int64,
		Company:       company.Int64,
		Day:           day.Int64,
		Status:        status.String,
		CreatedAt:     created_at.String,
		UpdatedAt:     updated_at.String,
	}, nil
}

func (r *OrderTarifRepo) Update(ctx context.Context, req *order_service.OrderTarifUpdate) (*order_service.OrderTarif, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"order_tarif"
		SET
		order_id = :order_id,
		tariff_payment = :tariff_payment,
		investor = :investor,
		insurance = :insurance,
		company = :company,
		day = :day,
		status = :status,
		updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":             req.Id,
		"order_id":       helper.NewNullString(req.OrderId),
		"tariff_payment": req.TariffPayment,
		"investor":       req.Investor,
		"insurance":      req.Insurance,
		"company":        req.Company,
		"day":            req.Day,
		"status":         req.Status,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &order_service.OrderTarif{
		Id:            req.Id,
		OrderId:       req.OrderId,
		TariffPayment: req.TariffPayment,
		Investor:      req.Investor,
		Insurance:     req.Insurance,
		Company:       req.Company,
		Day:           req.Day,
		Status:        req.Status,
	}, nil
}
