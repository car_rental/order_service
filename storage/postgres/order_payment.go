package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/order_service/genproto/order_service"
	"gitlab.com/car_rental/order_service/pkg/helper"
)

type OrderPaymentRepo struct {
	db *pgxpool.Pool
}

func NewOrderPaymentRepo(db *pgxpool.Pool) *OrderPaymentRepo {
	return &OrderPaymentRepo{
		db: db,
	}
}

func (r *OrderPaymentRepo) Create(ctx context.Context, req *order_service.OrderPaymentCreate) (*order_service.OrderPayment, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "order_payment"(
			id,
			order_id,
			cash,
			humo,
			uzcard,
			click, 
			payme,
			updated_at
		)
		VALUES ($1, $2, $3, $4, $5, $6, $7, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		helper.NewNullString(req.OrderId),
		req.Cash,
		req.Humo,
		req.Uzcard,
		req.Click,
		req.Payme,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.OrderPayment{
		Id:      id,
		OrderId: req.OrderId,
		Cash:    req.Cash,
		Humo:    req.Humo,
		Uzcard:  req.Uzcard,
		Click:   req.Click,
		Payme:   req.Payme,
	}, nil
}

func (r *OrderPaymentRepo) GetByID(ctx context.Context, req *order_service.OrderPaymentPrimaryKey) (*order_service.OrderPayment, error) {

	// var whereField = "id"
	// if len(req.Username) > 0 {
	// 	whereField = "username"
	// 	req.Id = req.Username
	// }

	var (
		query string

		id         sql.NullString
		order_id   sql.NullString
		cash       sql.NullInt64
		humo       sql.NullInt64
		uzcard     sql.NullInt64
		click      sql.NullInt64
		payme      sql.NullInt64
		created_at sql.NullString
		updated_at sql.NullString
	)

	query = `
		SELECT
		id,
		order_id,
		cash,
		humo,
		uzcard,
		click, 
		payme,
		created_at,
		updated_at		
		FROM "order_payment"
		WHERE id = $1
	`
	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&order_id,
		&cash,
		&humo,
		&uzcard,
		&click,
		&payme,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.OrderPayment{
		Id:        id.String,
		OrderId:   order_id.String,
		Cash:      cash.Int64,
		Humo:      humo.Int64,
		Uzcard:    uzcard.Int64,
		Click:     click.Int64,
		Payme:     payme.Int64,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}

func (r *OrderPaymentRepo) GetList(ctx context.Context, req *order_service.OrderPaymentGetListRequest) (*order_service.OrderPaymentGetListResponse, error) {

	var (
		resp   = &order_service.OrderPaymentGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			order_id,
			cash,
			humo,
			uzcard,
			click, 
			payme,
			created_at,
			updated_at		
		FROM "order_payment"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND order_id ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			order_id   sql.NullString
			cash       sql.NullInt64
			humo       sql.NullInt64
			uzcard     sql.NullInt64
			click      sql.NullInt64
			payme      sql.NullInt64
			created_at sql.NullString
			updated_at sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&order_id,
			&cash,
			&humo,
			&uzcard,
			&click,
			&payme,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.OrderPayments = append(resp.OrderPayments, &order_service.OrderPayment{
			Id:        id.String,
			OrderId:   order_id.String,
			Cash:      cash.Int64,
			Humo:      humo.Int64,
			Uzcard:    uzcard.Int64,
			Click:     click.Int64,
			Payme:     payme.Int64,
			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *OrderPaymentRepo) Update(ctx context.Context, req *order_service.OrderPaymentUpdate) (*order_service.OrderPayment, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"order_payment"
		SET
			order_id = :order_id,
			cash = :cash,
			humo = :humo,
			uzcard = :uzcard,
			click = :click,
			payme = :payme,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":       req.Id,
		"order_id": helper.NewNullString(req.OrderId),
		"cash":     req.Cash,
		"humo":     req.Humo,
		"uzcard":   req.Uzcard,
		"click":    req.Click,
		"payme":    req.Payme,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &order_service.OrderPayment{
		Id:      req.Id,
		OrderId: req.OrderId,
		Cash:    req.Cash,
		Humo:    req.Humo,
		Uzcard:  req.Uzcard,
		Click:   req.Click,
		Payme:   req.Payme,
	}, nil
}

func (r *OrderPaymentRepo) Delete(ctx context.Context, req *order_service.OrderPaymentPrimaryKey) error {

	_, err := r.db.Exec(ctx, `DELETE FROM "order_payment" WHERE id = $1`, req.Id)
	if err != nil {
		return err
	}

	return nil
}
