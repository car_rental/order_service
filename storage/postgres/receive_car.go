package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/order_service/genproto/order_service"
	"gitlab.com/car_rental/order_service/pkg/helper"
)

type RecieveCarRepo struct {
	db *pgxpool.Pool
}

func NewRecieveCarRepo(db *pgxpool.Pool) *RecieveCarRepo {
	return &RecieveCarRepo{
		db: db,
	}
}

func (r *RecieveCarRepo) Create(ctx context.Context, req *order_service.RecieveCarCreate) (*order_service.RecieveCar, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "recieve_car"(id, order_id, mechanik_id, mileage, gas, photo, created_at, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, NOW(), NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		helper.NewNullString(req.OrderId),
		helper.NewNullString(req.MechanicId),
		req.Mileage,
		req.Gas,
		req.Photo,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.RecieveCar{
		Id:         id,
		OrderId:    req.OrderId,
		MechanicId: req.MechanicId,
		Mileage:    req.Mileage,
		Gas:        req.Gas,
		Photo:      req.Photo,
	}, nil
}

func (r *RecieveCarRepo) GetByID(ctx context.Context, req *order_service.RecieveCarPrimaryKey) (*order_service.RecieveCar, error) {

	var (
		query string

		Id         sql.NullString
		OrderId    sql.NullString
		MechanicId sql.NullString
		Mileage    int64
		Gas        int64
		Photo      sql.NullString
		createdAt  sql.NullString
		updatedAt  sql.NullString
	)

	query = `
		SELECT
			id, order_id, mechanik_id, mileage, gas, photo, created_at, updated_at
		FROM "recieve_car"
		WHERE id = $1
	`
	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&OrderId,
		&MechanicId,
		&Mileage,
		&Gas,
		&Photo,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.RecieveCar{
		Id:         Id.String,
		OrderId:    OrderId.String,
		MechanicId: MechanicId.String,
		Mileage:    Mileage,
		Gas:        Gas,
		Photo:      Photo.String,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}, nil
}

func (r *RecieveCarRepo) GetList(ctx context.Context, req *order_service.RecieveCarGetListRequest) (*order_service.RecieveCarGetListResponse, error) {

	var (
		resp   = &order_service.RecieveCarGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id, order_id, mechanik_id, mileage, gas, photo, created_at, updated_at	
		FROM "recieve_car"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND mileage ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id         sql.NullString
			OrderId    sql.NullString
			MechanicId sql.NullString
			Mileage    int64
			Gas        int64
			Photo      sql.NullString
			createdAt  sql.NullString
			updatedAt  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&OrderId,
			&MechanicId,
			&Mileage,
			&Gas,
			&Photo,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.RecieveCars = append(resp.RecieveCars, &order_service.RecieveCar{
			Id:         Id.String,
			OrderId:    OrderId.String,
			MechanicId: MechanicId.String,
			Mileage:    Mileage,
			Gas:        Gas,
			Photo:      Photo.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *RecieveCarRepo) Update(ctx context.Context, req *order_service.RecieveCarUpdate) (*order_service.RecieveCar, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"recieve_car"
		SET
			id = :id,
			order_id = :order_id,
			mechanik_id = :mechanic_id,
			mileage = :mileage,
			gas = :gas,
			photo = :photo,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":          req.Id,
		"order_id":    helper.NewNullString(req.OrderId),
		"mechanic_id": helper.NewNullString(req.MechanicId),
		"mileage":     req.Mileage,
		"gas":         req.Gas,
		"photo":       req.Photo,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &order_service.RecieveCar{
		Id:         req.Id,
		OrderId:    req.OrderId,
		MechanicId: req.MechanicId,
		Mileage:    req.Mileage,
		Gas:        req.Gas,
		Photo:      req.Photo,
	}, nil
}

func (r *RecieveCarRepo) Delete(ctx context.Context, req *order_service.RecieveCarPrimaryKey) error {

	_, err := r.db.Exec(ctx, `DELETE FROM "recieve_car" WHERE id = $1`, req.Id)
	if err != nil {
		return err
	}

	return nil
}
