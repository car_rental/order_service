package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/order_service/config"
	"gitlab.com/car_rental/order_service/storage"
)

type Store struct {
	db           *pgxpool.Pool
	order        *OrderRepo
	tarif        *TarifRepo
	receiveCar   *RecieveCarRepo
	givecar      *GiveCarRepo
	orderpayment *OrderPaymentRepo
	ordertarif   *OrderTarifRepo
	debt         *DeptRepo
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, nil

}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Order() storage.OrderRepoI {
	if s.order == nil {
		s.order = NewOrderRepo(s.db)
	}

	return s.order
}

func (s *Store) Tarif() storage.TarifRepoI {
	if s.tarif == nil {
		s.tarif = NewTarifRepo(s.db)
	}

	return s.tarif
}

func (s *Store) RecieveCar() storage.RecieveCarRepoI {
	if s.receiveCar == nil {
		s.receiveCar = NewRecieveCarRepo(s.db)
	}

	return s.receiveCar
}

func (s *Store) GiveCar() storage.GiveCarRepoI {
	if s.givecar == nil {
		s.givecar = NewGiveCarRepo(s.db)
	}

	return s.givecar
}

func (s *Store) OrderPayment() storage.OrderPaymentRepoI {
	if s.orderpayment == nil {
		s.orderpayment = NewOrderPaymentRepo(s.db)
	}

	return s.orderpayment
}

func (s *Store) OrderTarif() storage.OrderTarifRepoI {
	if s.ordertarif == nil {
		s.ordertarif = NewOrderTarifRepo(s.db)
	}

	return s.ordertarif
}

func (s *Store) Debt() storage.DebtRepoI {
	if s.debt == nil {
		s.debt = NewDeptRepo(s.db)
	}

	return s.debt
}
