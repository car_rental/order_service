package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/order_service/genproto/order_service"
	"gitlab.com/car_rental/order_service/pkg/helper"
)

type DeptRepo struct {
	db *pgxpool.Pool
}

func NewDeptRepo(db *pgxpool.Pool) *DeptRepo {
	return &DeptRepo{
		db: db,
	}
}

func (r *DeptRepo) Create(ctx context.Context, req *order_service.DeptCreate) (*order_service.Dept, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "dept"(id, order_id, user_id, car_id, debt, updated_at)
		VALUES ($1, $2, $3, $4, $5, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		helper.NewNullString(req.OrderId),
		helper.NewNullString(req.UserId),
		helper.NewNullString(req.CarId),
		req.Amount,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.Dept{
		Id:      id,
		UserId:  req.UserId,
		CarId:   req.CarId,
		OrderId: req.OrderId,
		Amount:  req.Amount,
		Paid:    false,
	}, nil
}

func (r *DeptRepo) GetByID(ctx context.Context, req *order_service.DeptPrimaryKey) (*order_service.Dept, error) {
	var (
		query string

		Id        sql.NullString
		OrderId   sql.NullString
		UserId    sql.NullString
		CarId     sql.NullString
		Debt      int64
		Paid      bool
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)

	query = `
		SELECT
			id, order_id, user_id, car_id, debt, paid, created_at, updated_at	
		FROM "dept"
		WHERE id = $1
	`
	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&OrderId,
		&UserId,
		&CarId,
		&Debt,
		&Paid,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.Dept{
		Id:      Id.String,
		UserId:  UserId.String,
		CarId:   CarId.String,
		OrderId: OrderId.String,
		Amount:  Debt,
		Paid:    Paid,
	}, nil
}

func (r *DeptRepo) GetList(ctx context.Context, req *order_service.DeptGetListRequest) (*order_service.DeptGetListResponse, error) {

	var (
		resp   = &order_service.DeptGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			order_id,
			user_id,
			car_id,
			debt,
			paid,
			created_at,
			updated_at		
		FROM "dept"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			OrderId   sql.NullString
			UserId    sql.NullString
			CarId     sql.NullString
			Debt      int64
			Paid      bool
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&OrderId,
			&UserId,
			&CarId,
			&Debt,
			&Paid,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.DeptInfo = append(resp.DeptInfo, &order_service.Dept{
			Id:      Id.String,
			UserId:  UserId.String,
			CarId:   CarId.String,
			OrderId: OrderId.String,
			Amount:  Debt,
			Paid:    Paid,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *DeptRepo) Update(ctx context.Context, req *order_service.DeptUpdate) (*order_service.Dept, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"dept"
		SET
			order_id = :OrderId,
			user_id = :UserId,
			car_id = :CarId,
			debt = :Debt,
			paid = :Paid,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"OrderId": req.OrderId,
		"UserId":  req.UserId,
		"CarId":   req.CarId,
		"Debt":    req.Amount,
		"Paid":    req.Paid,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &order_service.Dept{
		Id:      req.Id,
		UserId:  req.UserId,
		CarId:   req.CarId,
		OrderId: req.OrderId,
		Amount:  req.Amount,
		Paid:    req.Paid,
	}, nil
}

func (r *DeptRepo) Delete(ctx context.Context, req *order_service.DeptPrimaryKey) error {

	_, err := r.db.Exec(ctx, `DELETE FROM "dept" WHERE id = $1`, req.Id)
	if err != nil {
		return err
	}

	return nil
}
