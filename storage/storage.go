package storage

import (
	"context"

	"gitlab.com/car_rental/order_service/genproto/order_service"
)

type StorageI interface {
	CloseDB()
	Order() OrderRepoI
	Tarif() TarifRepoI
	RecieveCar() RecieveCarRepoI
	GiveCar() GiveCarRepoI
	OrderPayment() OrderPaymentRepoI
	OrderTarif() OrderTarifRepoI
	Debt() DebtRepoI
}

type OrderRepoI interface {
	Create(context.Context, *order_service.OrderCreate) (*order_service.Order, error)
	GetByID(context.Context, *order_service.OrderPrimaryKey) (*order_service.Order, error)
	GetList(context.Context, *order_service.OrderGetListRequest) (*order_service.OrderGetListResponse, error)
	Update(context.Context, *order_service.OrderUpdate) (*order_service.Order, error)
	Delete(context.Context, *order_service.OrderPrimaryKey) error
}

type OrderTarifRepoI interface {
	Create(context.Context, *order_service.OrderTarifCreate) (*order_service.OrderTarif, error)
	GetByID(context.Context, *order_service.OrderTarifPrimaryKey) (*order_service.OrderTarif, error)
	//GetList(context.Context, *order_service.OrderTarifGetListRequest) (*order_service.OrderTarifGetListResponse, error)
	Update(context.Context, *order_service.OrderTarifUpdate) (*order_service.OrderTarif, error)
	// Delete(context.Context, *order_service.OrderTarifPrimaryKey) error
}

type TarifRepoI interface {
	Create(context.Context, *order_service.TarifCreate) (*order_service.Tarif, error)
	GetByID(context.Context, *order_service.TarifPrimaryKey) (*order_service.Tarif, error)
	GetList(context.Context, *order_service.TarifGetListRequest) (*order_service.TarifGetListResponse, error)
	Update(context.Context, *order_service.TarifUpdate) (*order_service.Tarif, error)
	Delete(context.Context, *order_service.TarifPrimaryKey) error
}

type RecieveCarRepoI interface {
	Create(context.Context, *order_service.RecieveCarCreate) (*order_service.RecieveCar, error)
	GetByID(context.Context, *order_service.RecieveCarPrimaryKey) (*order_service.RecieveCar, error)
	GetList(context.Context, *order_service.RecieveCarGetListRequest) (*order_service.RecieveCarGetListResponse, error)
	Update(context.Context, *order_service.RecieveCarUpdate) (*order_service.RecieveCar, error)
	Delete(context.Context, *order_service.RecieveCarPrimaryKey) error
}

type GiveCarRepoI interface {
	Create(context.Context, *order_service.GiveCarCreate) (*order_service.GiveCar, error)
	GetByID(context.Context, *order_service.GiveCarPrimaryKey) (*order_service.GiveCar, error)
	GetList(context.Context, *order_service.GiveCarGetListRequest) (*order_service.GiveCarGetListResponse, error)
	Update(context.Context, *order_service.GiveCarUpdate) (*order_service.GiveCar, error)
	Delete(context.Context, *order_service.GiveCarPrimaryKey) error
}

type OrderPaymentRepoI interface {
	Create(context.Context, *order_service.OrderPaymentCreate) (*order_service.OrderPayment, error)
	GetByID(context.Context, *order_service.OrderPaymentPrimaryKey) (*order_service.OrderPayment, error)
	GetList(context.Context, *order_service.OrderPaymentGetListRequest) (*order_service.OrderPaymentGetListResponse, error)
	Update(context.Context, *order_service.OrderPaymentUpdate) (*order_service.OrderPayment, error)
	Delete(context.Context, *order_service.OrderPaymentPrimaryKey) error
}

type DebtRepoI interface {
	Create(context.Context, *order_service.DeptCreate) (*order_service.Dept, error)
	GetByID(context.Context, *order_service.DeptPrimaryKey) (*order_service.Dept, error)
	GetList(context.Context, *order_service.DeptGetListRequest) (*order_service.DeptGetListResponse, error)
	Update(context.Context, *order_service.DeptUpdate) (*order_service.Dept, error)
	Delete(context.Context, *order_service.DeptPrimaryKey) error
}
